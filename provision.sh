#!/bin/sh

# install java
sudo apt-get install default-jdk -y

# install elasticsearch
wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.2.2.deb
sudo dpkg -i elasticsearch-1.2.2.deb

# install mavel
/usr/share/elasticsearch/bin/plugin -i elasticsearch/marvel/latest

# restart elasticsearch
sudo service elasticsearch restart